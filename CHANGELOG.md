# Go AST Scanner analyzer changelog

GitLab Go AST Scanner analyzer follows versioning of GitLab (`MAJOR.MINOR` only) and generates a `MAJOR-MINOR-stable` [Docker image](https://gitlab.com/gitlab-org/security-products/analyzers/go-ast-scanner/container_registry).

These "stable" Docker images may be updated after release date, changes are added to the corresponding section bellow.

## 11-2-stable

## 11-1-stable
- Show command error output

## 11-0-stable
- Enrich report with more data

## 10-8-stable
- Rewrite using Go and analyzers common library

## v0.1.0
- initial release
